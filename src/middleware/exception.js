const { v4: uuidv4 } = require('uuid');

const exceptionMiddleware = (err, req, res) => {
    if (res.headersSent) return;

    if (err.message && err.statusCode) {
        return res.status(err.statusCode).json({
            error: err.message,
        });
    }

    const uuid = uuidv4();
    console.error(`requestId=${uuid} err=${err}`);

    return res.status(500).send({
        error: `unexpected error. requestId: ${uuid}`,
    });
}

module.exports = exceptionMiddleware;