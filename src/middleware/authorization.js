const { UnauthorizedException, ForbiddenException } = require("../model/exceptions");
const jwt = require('jsonwebtoken');

const requireAuthorization = (req, res, next, context) => {
    const header = req.header('Authentication');
    if (!header) {
        next(new UnauthorizedException('Access Token not found'));
        return;
    }
    const auth = header.replace(/^Bearer\s/, '');
    
    // We're literally ignoring the error.
    jwt.verify(auth, context.secret, (_, openToken) => {
        if (!openToken) {
            next(new UnauthorizedException('invalid token'));
            return;
        }

        if (openToken.id !== req.params.uuid) {
            next(new ForbiddenException('Access Token did not match User ID'));
            return;
        }

        next();
    });
}

module.exports = requireAuthorization;