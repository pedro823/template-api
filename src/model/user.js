const { v4: uuidv4 } = require('uuid');
const { emailIsValid } = require('../util/email');

// We want to distinguish empty strings from null/undefined fields.
const isNullOrUndefined = (x) => x === null || x === undefined;

const isValidPassword = (pass) => pass.length >= 8 && pass.length <= 32 && /^[a-zA-Z1-9]+$/.test(pass);

class ModelValidationError extends Error {}
class PasswordConfirmationError extends Error {}

class User {
    constructor(obj = this.initialValues(), requireConfirmation = false) {
        Object.assign(this, obj);
        this.uuid = uuidv4();
        this.requireConfirmation = requireConfirmation;
    }

    initialValues() {
        return {
            name: null,
            email: null,
            password: null,
        };
    }

    validate() {
        for (let field of Object.keys(this.initialValues())) {
            if (isNullOrUndefined(this[field])) {
                throw new ModelValidationError(`Request body had missing field ${field}`);
            }
        }

        if (!emailIsValid(this.email)) {
            throw new ModelValidationError(`Request body had malformed field email`);
        }

        if (!isValidPassword(this.password)) {
            throw new ModelValidationError(`Request body had malformed field password`);
        }

        if ((this.requireConfirmation || this.passwordConfirmation) && this.passwordConfirmation != this.password) {
            throw new PasswordConfirmationError('Password confirmation did not match');
        }
    }
}


module.exports = {
    User,
    ModelValidationError,
    PasswordConfirmationError,
}