class RequestException extends Error {
    constructor(statusCode, message) {
        super(message);
        this.statusCode = statusCode;
    }
}

class BadRequestException extends RequestException {
    constructor(message) {
        super(400, message);
    }
}

class UnauthorizedException extends RequestException {
    constructor(message) {
        super(401, message);
    }
}

class ForbiddenException extends RequestException {
    constructor(message) {
        super(403, message);
    }
}

class UnprocessableEntityException extends RequestException {
    constructor(message) {
        super(422, message);
    }
}

module.exports = {
    RequestException,
    BadRequestException,
    UnauthorizedException,
    ForbiddenException,
    UnprocessableEntityException,
};