const { createUserHandler, deleteUserHandler } = require('./user');

module.exports = {
    createUserHandler,
    deleteUserHandler,
}