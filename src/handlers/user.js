const { BadRequestException, UnprocessableEntityException } = require("../model/exceptions");
const { User, ModelValidationError, PasswordConfirmationError } = require("../model/user");

const createUser = async (body, mongodb, nc) => {
    const user = new User(body, true);
    user.validate();
    delete user.requireConfirmation;

    const database = mongodb.db('template-api');
    const collection = database.collection('users');

    const query = { email: user.email }
    const userInDb = await collection.findOne(query)

    if (userInDb) {
        throw BadRequestException('User is already registered');
    }

    nc.publish('userCreated', JSON.stringify({
        eventType: 'userCreated',
        entityId: user.uuid,
        entityAggregate: {
            name: user.name,
            email: user.email,
            password: user.password,
        },
    }));

    return user;
}

const removeSensitiveFields = (user) => {
    delete user.password;
    delete user.passwordConfirmation;
    return user;
}

const createUserHandler = async (req, res, next, { mongoClient, stanConn }) => {
    const body = req.body;

    try {
        const user = removeSensitiveFields(await createUser(body, mongoClient, stanConn));

        res.status(201);
        res.json({
            user: {
                id: user.uuid,
                name: user.name,
                email: user.email,
            },
        });
    } catch (ex) {
        if (ex instanceof ModelValidationError) {
            next(new BadRequestException(ex.message));
            return;
        }
        if (ex instanceof PasswordConfirmationError) {
            next(new UnprocessableEntityException(ex.message));
            return;
        }

        next(ex);
    }
};

const deleteUser = async (userId, mongodb, nc) => {
    nc.publish('userDeleted', JSON.stringify({
        eventType: 'userDeleted',
        entityId: userId,
        entityAggregate: {},
    }));
}

const deleteUserHandler = async (req, res, next, { mongoClient, stanConn }) => {
    const userId = req.params.uuid;
    await deleteUser(userId, mongoClient, stanConn);

    res.send({ id: userId });
    next();
}

module.exports = {
    createUser,
    createUserHandler,
    deleteUser,
    deleteUserHandler,
};