const stan = require("node-nats-streaming");

module.exports = function () {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  return conn;
};
