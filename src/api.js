const express = require("express");
const cors = require("cors");
const { createUserHandler, deleteUserHandler } = require('./handlers');
const exceptionMiddleware = require('./middleware/exception');
const requireAuthorization = require('./middleware/authorization');

const contextualize = (context) => (f) => (req, res, next) => f(req, res, next, context);

module.exports = function (corsOptions, context) {
  const withContext = contextualize(context);

  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  api.post('/users', withContext(createUserHandler));

  api.delete('/users/:uuid', withContext(requireAuthorization), withContext(deleteUserHandler));

  api.use((err, req, res, next) => exceptionMiddleware(err, req, res, next));

  return api;
};
